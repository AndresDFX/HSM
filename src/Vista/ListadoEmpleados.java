/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Controladora.Control;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sebas
 */
public class ListadoEmpleados extends javax.swing.JFrame {
    Control control = new Control();
    

    /**
     * Creates new form Listado
     */
    public ListadoEmpleados() {
        initComponents();
        control.mostrarAreasCombo(comboArea);
        this.setLocationRelativeTo(null);
        this.setIconImage(VentanaLogin.iconoVentana); 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        comboArea = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        Listado = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Listado");
        setUndecorated(true);

        jLabel1.setText("Área:");

        comboArea.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un área" }));
        comboArea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboAreaItemStateChanged(evt);
            }
        });
        comboArea.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboAreaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                comboAreaFocusLost(evt);
            }
        });
        comboArea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                comboAreaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                comboAreaMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                comboAreaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                comboAreaMouseReleased(evt);
            }
        });
        comboArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboAreaActionPerformed(evt);
            }
        });

        jButton1.setText("Buscar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        Listado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Cedula", "Nombre", "Cargo", "Email"
            }
        ));
        jScrollPane2.setViewportView(Listado);

        jButton2.setText("Menú");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addComponent(jLabel1)
                .addGap(40, 40, 40)
                .addComponent(comboArea, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 164, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(108, 108, 108))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(301, 301, 301))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 457, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            String codigo, area;
            String[] codigoSolo;
            codigo = (String)this.comboArea.getSelectedItem();
            codigoSolo = codigo.split(",");
            ResultSet resultado = null;
            resultado = control.listarEmpleados(codigoSolo[0]);
            
            
            if(resultado.next()){
                resultado = control.listarEmpleados(codigoSolo[0]);
                ResultSetMetaData result = resultado.getMetaData();
                 int numero = result.getColumnCount();
                
                DefaultTableModel modelo = new DefaultTableModel();
            
                modelo.addColumn("Cedula");
                modelo.addColumn("Nombre");
                modelo.addColumn("Cargo");
                modelo.addColumn("Email");
                while(resultado.next()){
                    
                    Object[] filas = new Object[numero];
                    for(int i=0; i < numero; i++){
                        filas[i] = resultado.getObject(i+1);
                    }
                modelo.addRow(filas);
                }
                Listado.setModel(modelo);
            }else{
                JOptionPane.showMessageDialog(null, "No hay empleados en este área", "Listar empleados", JOptionPane.INFORMATION_MESSAGE);
            }
            
        } catch (SQLException ex) {
            System.out.println("Excepcion imprimiendo los empleados");
            
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void comboAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboAreaActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaActionPerformed

    private void comboAreaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboAreaFocusGained
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaFocusGained

    private void comboAreaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboAreaMouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaMouseClicked

    private void comboAreaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboAreaFocusLost
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaFocusLost

    private void comboAreaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboAreaItemStateChanged
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaItemStateChanged

    private void comboAreaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboAreaMousePressed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaMousePressed

    private void comboAreaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboAreaMouseReleased
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaMouseReleased

    private void comboAreaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboAreaMouseEntered
        // TODO add your handling code here:
        
    }//GEN-LAST:event_comboAreaMouseEntered

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.setVisible(false);
        new VistaAdministrador().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Listado;
    private javax.swing.JComboBox<String> comboArea;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
